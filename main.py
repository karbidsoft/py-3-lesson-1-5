def init_data():
  students = [
    {'name': 'Alex', 'sure_name': 'Petrov', 'sex': 'male', 'experience': True, 'homework': [8, 6, 5, 7, 9], 'exam': 2},
    {'name': 'Ivan', 'sure_name': 'Petrov', 'sex': 'male', 'experience': False, 'homework': [7, 3, 1, 2, 10], 'exam': 6},
    {'name': 'Andrey', 'sure_name': 'Galkin', 'sex': 'male', 'experience': True, 'homework': [5, 6, 2, 9, 7], 'exam': 3},
    {'name': 'Olga', 'sure_name': 'Leha', 'sex': 'female', 'experience': True, 'homework': [6, 7, 3, 4, 8], 'exam': 5},
    {'name': 'Galya', 'sure_name': 'Petina', 'sex': 'female', 'experience': True, 'homework': [6, 4, 4, 2, 1], 'exam': 7},
    {'name': 'Marina', 'sure_name': 'Zayaceva', 'sex': 'female', 'experience': True, 'homework': [2, 3, 5, 6, 8], 'exam': 9},
    {'name': 'Vasya', 'sure_name': 'Pupkin', 'sex': 'male', 'experience': False, 'homework': [8, 7, 8, 6, 4], 'exam': 9},
    {'name': 'Petya', 'sure_name': 'Dudin', 'sex': 'male', 'experience': True, 'homework': [8, 7, 8, 6, 4], 'exam': 9},
    {'name': 'Volodya', 'sure_name': 'Putin', 'sex': 'male', 'experience': False, 'homework': [6, 9, 1, 5, 8], 'exam': 1},
    {'name': 'Dima', 'sure_name': 'Medvedev', 'sex': 'male', 'experience': False, 'homework': [6, 3, 1, 5, 6], 'exam': 1}
    ]
  return students
  
def calc_avg_appraisal(sex=False, exp=False):
  students = init_data()
  homework_max = 0
  for student in students:
    if homework_max < len(student['homework']):
      homework_max = len(student['homework'])
  if sex:
    for homework_num in range(homework_max):
      avg_appraisal_male = 0
      avg_appraisal = 0
      len_male = 0
      for student in students:
        if student['sex'] == 'male':
          try:
            avg_appraisal_male += student['homework'][homework_num]
            len_male += 1
          except Exception:
            avg_appraisal_male += 0
    
        else:
          try:
            avg_appraisal += student['homework'][homework_num]
          except Exception:
            avg_appraisal += 0
      print('Средний балл за задание {} среди мужчин равен: {::=.3}'. format(homework_num + 1, avg_appraisal_male / len_male))
      print('Средний балл за задание {} среди женщин равен: {::=.3}'. format(homework_num + 1, avg_appraisal / (len(students) - len_male)))
  elif exp:
    for homework_num in range(homework_max):
      avg_appraisal_exp = 0
      avg_appraisal = 0
      len_exp = 0
      for student in students:
        if student['experience']:
          try:
            avg_appraisal_exp += student['homework'][homework_num]
            len_exp += 1
          except Exception:
            avg_appraisal_exp += 0
        else:
          try:
            avg_appraisal += student['homework'][homework_num]
          except Exception:
            avg_appraisal += 0
      print('Средний балл за задание {} по студентам с опытом равен: {::=.3}'. format(homework_num + 1, avg_appraisal_exp / len_exp))
      print('Средний балл за задание {} по студентам без опыта равен: {::=.3}'. format(homework_num + 1, avg_appraisal / (len(students) - len_exp)))
  else:
    for homework_num in range(homework_max):
      avg_appraisal = 0
      for student in students:
        try:
          avg_appraisal += student['homework'][homework_num]
        except Exception:
          avg_appraisal += 0
      print('Средний балл за задание {} по всем студентам равен: {::=.3}'. format(homework_num + 1, avg_appraisal / len(students)))

def calc_avg_exam(sex=False, exp=False):
  students = init_data()
  avg_exam = 0
  avg_exam_exp = 0
  avg_exam_male = 0
  len_male = 0
  len_exp = 0
  if exp:
    for student in students:
      if student['experience']:
        avg_exam_exp += student['exam']
        len_exp += 1
      else:
        avg_exam += student['exam']
    print('Средний балл за экзамен по студентам с опытом: {::=.3}' .format(avg_exam_exp / len_exp))
    print('Средний балл за экзамен по студентам без опыта: {::=.3}' .format(avg_exam / (len(students) - len_exp)))
  elif sex:
    for student in students:
      if student['sex'] == 'male':
        avg_exam_male += student['exam']
        len_male += 1
      else:
        avg_exam += student['exam']
    print('Средний балл за экзамен по студентам мужчинам: {::=.3}' .format(avg_exam_male / len_male))
    print('Средний балл за экзамен по студентам женщинам: {::=.3}' .format(avg_exam / (len(students) - len_male)))
  else:
    for student in students:
      avg_exam += student['exam']
    print('Средний балл за экзамен по всем студентам: {::=.3}' .format(avg_exam / len(students)))
  
def best_student():
  students = init_data()
  best_student_name = {'1': []} 
  student_appraisal = 0
  for student in students:
    student_appraisal_temp = (0.6 * (sum(student['homework']) / len(student['homework']))) + (0.4 * student['exam'])
    if student_appraisal < student_appraisal_temp:
      best_student_name['1'] = student['name'], student['sure_name'], student_appraisal_temp
      student_appraisal = student_appraisal_temp
    elif student_appraisal == student_appraisal_temp:
      best_student_name[str(len(best_student_name)+1)] = student['name'], student['sure_name'], student_appraisal_temp
      student_appraisal = student_appraisal_temp
  #print(best_student_name)
  for pos, student in best_student_name.items():
    print('Лучший студент #{}: {} {} с баллом {}'.format(*pos, *student))

def calc_avg_appraisal_all(sex=False, exp=False):
  students = init_data()
  avg_appraisal = 0
  avg_appraisal_male = 0
  avg_appraisal_exp = 0
  len_male = 0
  len_exp = 0
  if sex:
    for student in students:
      if student['sex'] == 'male':
        avg_appraisal_male += sum(student['homework'])
        len_male += 1
      else:
        avg_appraisal += sum(student['homework'])
    avg_appraisal_male = avg_appraisal_male / (len_male * len(student['homework']))
    avg_appraisal = avg_appraisal / ((len(students) - len_male) * len(student['homework']))
    print('Средний балл за домашние задания среди мужчин равен: {::=.3}'. format(avg_appraisal_male))
    print('Средний балл за домашние задания среди женщин равен: {::=.3}'. format(avg_appraisal))

  elif exp:
    for student in students:
      if student['experience']:
        avg_appraisal_exp += sum(student['homework'])
        len_exp += 1
      else:
        avg_appraisal += sum(student['homework'])
    avg_appraisal_exp = avg_appraisal_exp / (len_exp * len(student['homework']))
    avg_appraisal = avg_appraisal / ((len(students) - len_exp) * len(student['homework']))
    print('Средний балл за домашние задания у студентов с опытом: {::=.3}'. format(avg_appraisal_exp))
    print('Средний балл за домашние задания у студентов без опыта: {::=.3}'. format(avg_appraisal))

  else:
    for student in students:
      avg_appraisal += sum(student['homework'])
    avg_appraisal = avg_appraisal / (len(students) * len(student['homework']))
    print('Средний балл за домашние задания у всех студентов: {::=.3}'. format(avg_appraisal))
  

while True:
  print('--------------------------------------------------------------------------\n')
  print('Команды для работы с базой данных студентов:\n')
  print('avg - Показывает средний балл по всем студентам в разрезе заданий')
  print('avg_all - Показывает средний балл за домашние задания по всей группе')
  print('avg_sex - Показывает средний балл в разрезе пола и задания')
  print('avg_exp - Показывает средний балл в разрезе опыта и задания')
  print('avg_sex_all - Показывает средний балл за все задания в разрезе пола')
  print('avg_exp_all - Показывает средний балл за все задания в разрезе опыта')
  print('best - Определение лучшего студента')
  print('--------------------------------------------------------------------------\n')
  command = input('Введите команду:')
  if command.lower() == 'avg':
    print('========================================================================')
    calc_avg_appraisal()
    calc_avg_exam()
  elif command.lower() == 'avg_sex':
    print('========================================================================')
    calc_avg_appraisal(sex=True)
    calc_avg_exam(sex=True)
  elif command.lower() == 'avg_exp':
    print('========================================================================')
    calc_avg_appraisal(exp=True)
    calc_avg_exam(exp=True)
  elif command.lower() == 'best':
    print('========================================================================')
    best_student()
  elif command.lower() == 'avg_all':
    print('========================================================================')
    calc_avg_appraisal_all()
    calc_avg_exam()
  elif command.lower() == 'avg_sex_all':
    print('========================================================================')
    calc_avg_appraisal_all(sex=True)
    calc_avg_exam(sex=True)
  elif command.lower() == 'avg_exp_all':
    print('========================================================================')
    calc_avg_appraisal_all(exp=True)
    calc_avg_exam(exp=True)